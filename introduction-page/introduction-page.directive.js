angular.module('introduction-page', ['service.user'])
.directive('introductionPage', ['User', '$state', function (User, $state) {
  'use strict';
  return {
    templateUrl: 'introduction-page/introduction-page.html',
    restrict: 'E',
    link: function(scope) {
      scope.authenticated = false;
      User.requestAuth("nothing").then(function (authMessage) {
        if (!authMessage) {
          $state.go('login.login');
        } else {
          scope.authenticated = true;
        }

      });
      scope.workbook = function () {
        window.open('data/resources/THE 90 DAY GAME PLAN Workbook(with cover).pdf', '_blank');
      }
    }
  }
}]);
