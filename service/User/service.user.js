angular.module('service.user', [])
  .factory('User', ['$http', '$q', function ($http, $q) {

    var _test = "The TEST";
    var _authenticated = false;
    var _authPromise;
    function authenticateLogin(loginObj) {
      return $http({
          method: 'POST',
          url: 'service/User/auth-login.php',
          data: loginObj,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
          switch(response.data) {
            case '200':
              return _authenticated = true;
              break;
            case '202':
              return _authenticated = true;
              break;
            case '401':
              return _authenticated = false;
              break;
            case '433':
              return _authenticated = false;
              break;
            default:
              _authenticated = false;
          }
      });
    };

    function getUserData() {
      return $http.get('service/User/getUserData.php').then(function (data) {
        var userObj = {
          name: data.data[0],
          userId: data.data[1],
          userEmail: data.data[2],
          completedQuestions: data.data[3]
        }
        return userObj;
      });
    }

    return {
      getTest: function () {
        return _test;
      },
      setTest: function (newValue) {
        _test = newValue;
      },
      requestAuth: function (loginObj) {
        return authenticateLogin(loginObj);
      },
      requestUser: function () {
        return getUserData();
      }
    };
  }]);
