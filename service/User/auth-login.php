<?php
  session_start();
  include '../../database/config.php';

  $data = json_decode(file_get_contents("php://input"));
  if (isset($_SESSION['userName'])) {
    echo 200;
  } else {
    $user_email = $data->email;
    $user_password = $data->password;


    // To protect MySQL injection for Security purpose
    $user_email = stripslashes($user_email);
    $user_password = stripslashes($user_password);

    // Convert user input to sha1 to be able match database query.
    $user_password = sha1($user_password);

    $query = "SELECT * FROM User_Table WHERE Email='$user_email'";

    $result = $link->query($query);
    $user_data = $result->fetch_array(MYSQLI_NUM);
    $row = mysqli_num_rows($result);


    if ($row == 1) {
      if ($user_data[5] == $user_password) {
          $query = "INSERT INTO login_log (EntryTime, UserId, Email, Status) VALUES (CURRENT_TIMESTAMP, '$user_data[1]', '$user_email', 'success')";
          $link->query($query);
          $_SESSION['login_errors'] = "Password matched! Successfully logged in!";
          $_SESSION['userName'] = $user_data[2];
          $_SESSION['userEmail'] = $user_email;
          $_SESSION['userId'] = $user_data[1];
          $_SESSION['completedQuestions'] = $user_data[8];
          echo 202;

      } else {
          $query = "INSERT INTO login_log (EntryTime, UserId, Email, Status) VALUES (CURRENT_TIMESTAMP, '$user_data[1]', '$user_email', 'wrong password')";
          $link->query($query);
          $_SESSION['login_errors'] = "Wrong Password or username!";
          // header("location: login.php");
          echo 401;
      }
    } else {
      $query = "INSERT INTO login_log (EntryTime, UserId, Email, Status) VALUES (CURRENT_TIMESTAMP, 'unkown', '$user_email', 'unknown user')";
      $link->query($query);
      $_SESSION['login_errors'] = "Wrong Password or username!";
      // header("location: login.php");
      echo 433;
    }
    $result->close();
    $link->close();
  }
?>
