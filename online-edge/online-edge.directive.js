angular.module('online-edge', ['ngSanitize', 'ngMaterial'])
.directive('onlineEdge', ['$http', '$sce', function ($http, $sce){
  'use strict';

  return {
    templateUrl: 'online-edge/online-edge.html',
    restrict: 'E',
    link: function (scope) {

      scope.showTech = false;

      $http.get('online-edge/get-online-edge.php').
        success(function(data) {
          scope.onlineEdge = {};
          scope.onlineEdge.toolslist = [];
          scope.onlineEdge.strategyOne = [];
          scope.onlineEdge.strategyOneTwelve = [];
          scope.onlineEdge.techyStuff = [];
          scope.onlineEdge.techyStuff[0] = {title: 'Who is your target audience?', content: []};
          scope.onlineEdge.techyStuff[1] = {title: 'Creating your lead magnet', content: []};
          scope.onlineEdge.techyStuff[2] = {title: 'Copywriting', content: []};
          scope.onlineEdge.techyStuff[3] = {title: 'Optimize your blog for opt-ins', content: []};
          scope.onlineEdge.techyStuff[4] = {title: 'Traffic generation for your blog', content: []};

          data.forEach( function (obj) {
            obj.embeded = false;
            switch(obj.category) {
              case "toolslist":
                scope.onlineEdge.toolslist.push(obj);
                break;
              case "strategyOne":
                scope.onlineEdge.strategyOne.push(obj);
                break;
              case "strategyOneTwelve":
                scope.onlineEdge.strategyOneTwelve.push(obj);
                break;
              case "techyOne":
                scope.onlineEdge.techyStuff[0].content.push(obj);
                break;
              case "techyTwo":
                scope.onlineEdge.techyStuff[1].content.push(obj);
                break;
              case "techyThree":
                scope.onlineEdge.techyStuff[2].content.push(obj);
                break;
              case "techyFour":
                scope.onlineEdge.techyStuff[3].content.push(obj);
                break;
              case "techyFive":
                scope.onlineEdge.techyStuff[4].content.push(obj);
                break;
            }
          });
        }).
        error(function(data) {
        });

      scope.addActivity = function (title, type, url) {
        var videoObj = {
  				title: title,
          type: type,
          url: url
  			};
  			$http({
            method: 'POST',
            url: 'online-edge/post-activity.php',
            data: JSON.stringify(videoObj),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response) {
        });
      }

      scope.trustSrc = function(src) {
        var url = src.replace('watch?v=', 'embed/');
        return $sce.trustAsResourceUrl(url);
      }
      scope.youTubeId = function (url) {
        return url.replace('https://www.youtube.com/watch?v=', '');
      }
    }
  }
}]);
