<?php
  session_start();
  if (!(isset($_SESSION['userEmail'])))  {
    header ("location: ../login-page/login.php");
    // $user_email = $_SESSION['userEmail'];
  } else {
    $user_name = $_SESSION['userName'];
    $user_email = $_SESSION['userEmail'];
    $user_id = $_SESSION['userId'];
  }
  include '../phpcomponents/links.php';
?>
<!DOCTYPE html>
<html ng-app="tabsDemoDynamicTabs">
  <head>
    <title>Questions</title>
    <link rel="stylesheet" href="tabs.css">
  </head>
  <body ng-controller="AppCtrl as ctrl" >
  <div class="questions-container" layout="row" >
    <div class="sample" layout="column" flex>
      <md-content class="md-padding">
        <md-tabs md-selected="selectedIndex" md-border-bottom>
          <md-tab ng-repeat="tab in tabs"
                  ng-disabled="tab.disabled"
                  label="<i ng-class='{checkmark: tab.completed}'></i>{{tab.topicTitle}}">
            <div class="content-container" layout="column" layout-align="start Start">
              <div class="input-intro-text" ng-bind-html="trustThisHTML(tab.topicIntroText)"></div>
                <div ng-repeat="question in tab.questions">
                  <div ng-switch="question.type">

                  <!-- BEGIN Long answer layout  -->
                  <div ng-switch-when="LS">
                    <div>
                      {{question.description}}
                      <md-input-container>
                        <label>Answer</label>
                        <textarea ng-model="question.answer.theAnswer" columns="1"></textarea>
                      </md-input-container>
                    </div>
                  </div>
                  <!-- END Long answer layout  -->

                  <!-- BEGIN Short answer layout  -->
                  <div ng-switch-when="SS">
                    <div>
                      <md-input-container>
                        <label>{{question.description}}</label>
                        <input ng-model="question.answer.theAnswer">
                      </md-input-container>
                    </div>
                  </div>
                  <!-- END Short answer layout  -->

                  <!-- BEGIN Multiple answers layout  -->
                  <div ng-switch-when="MA" layout="column" layout-align="start start">
                      <div layout="row">
                        {{question.description}}
                        <md-input-container>
                          <!-- <label>{{question.placeholder}}</label> -->
                          <input ng-model="thisAnswer">
                        </md-input-container>
                        <md-button ng-click="addMA(thisAnswer, question.topicId ,question.questionId); thisAnswer=''" class="md-raised md-primary" ng-disabled="thisAnswer === undefined || thisAnswer === ''">
                          {{question.placeholder2}}
                        </md-button>
                      </div>
                      <div layout="row">
                        <div class="my-chip" ng-repeat="answer in question.answer">
                          {{answer.theAnswer}}
                        </div>
                      </div>
                  </div>
                  <!-- END Multiple answers layout  -->

                  <!-- BEGIN Radio Button layout  -->
                  <div ng-switch-when="RB">
                    {{question.description}}
                    <md-radio-group ng-model="question.answer.theAnswer">
                      <md-radio-button ng-repeat="radioBtn in question.radioButtons"
                                        ng-value="radioBtn.radio">{{radioBtn.radio}}
                      </md-radio-button>
                    </md-radio-group>
                  </div>
                  <!-- END Radio Button layout  -->

                </div>
                <!-- END SWITCH STATEMENT  -->
              </div>
              <!-- END OF ng-repeat  -->
              <div style="text-align:center;">
                <md-button ng-click="postAnswers(tab)" class="md-raised md-primary">Submit My Answers</md-button>
                <div class="error-messages" id="error-message-{{tab.topicId}}"></div>
              </div>
            </div>
          </md-tab>
        </md-tabs>
      </md-content>
    </div>
  </div>
    <?php include '../phpcomponents/angular-js.php' ?>
    <script src="questions-angular.js"></script>
  </body>
</html>
