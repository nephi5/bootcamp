<?php
  session_start();
  include '../database/config.php';
  $user_id = $_SESSION['userId'];

  $data = json_decode(file_get_contents("php://input"));
  foreach ($data as &$obj) {
    $questionId = $obj->questionId;
    $answer = $obj->answer;
    if (is_array($answer)) {
      foreach ($answer as &$individual_answer) {
        $indv_answer = $individual_answer->theAnswer;
        // If the answer object has a property called answerId then we want to just update those answers.
        if (property_exists($individual_answer, "answerId")) {
          $indv_answer_id = $individual_answer->answerId;
          $update_answer = "UPDATE begin_answers SET answer='$indv_answer' WHERE answerId = '$indv_answer_id'";
          if ($link->query($update_answer)) {
            echo "Successfully Updated";
          } else {
            echo "Update fail";
          }
        // If it doesn't then we want to insert the answer that gets passed through
        } else {
          include '../database/generate_uuid.php';
          $insert_answer = "INSERT INTO begin_answers (questionId, userId, answer, answerId, entryDate) VALUES ('$questionId', '$user_id', '$indv_answer', '$uuid', CURRENT_TIMESTAMP)";
          if ($link->query($insert_answer)) {
            echo "Successfully added data";
          }
        }
      }
    } else {
      $the_answer = $answer->theAnswer;
      if (property_exists($answer, "answerId")) {
        $the_answer_id = $answer->answerId;
        $update_answer2 = "UPDATE begin_answers SET answer = '$the_answer' WHERE answerId = '$the_answer_id'";
        if ($link->query($update_answer2)) {
          echo " Updated Successfully ";
        }
      } else {
        include '../database/generate_uuid.php';
        $insert_answer2 = "INSERT INTO begin_answers (questionId, userId, answer, answerId, entryDate) VALUES ('$questionId', '$user_id', '$the_answer', '$uuid', CURRENT_TIMESTAMP)";
        if ($link->query($insert_answer2)) {
          echo "Successfully added data";
        } else {
          echo "For some reason that didn't work";
        }
      }
    }
  }
  $link->close();
?>
