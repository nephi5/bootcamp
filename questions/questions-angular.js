'use strict';
var app = angular.module('tabsDemoDynamicTabs', ['ngMaterial', 'ngSanitize']);
app.controller('AppCtrl', ['$scope', '$http', '$sce', function($scope, $http, $sce) {

  $http.get('/gitRepos/bootcamp/questions/process-questions.php').
  success(function(data) {
    var tabs = data;
    var selected = null;
    var previous = null;

    $scope.selectedIndex = 0;

    // If the tabs come back with answers I need to make sure I dont overwrite them with an empty array.
    // I need to initialize an array so I can push on it
    tabs.forEach(function(obj) {
      obj.questions.forEach(function(questionObj) {
        if (questionObj.type === 'MA') {
          if (!(questionObj.answer.length > 0)) {
            questionObj.answer = [];
          }
        } else if (questionObj.type === 'RB') {
          questionObj.radioButtons = radioButtonBuilder(questionObj.typeDetails);
        }
      });
      var counter = checkAnswers(obj);
      if (counter === 0) {
        obj.completed = true;
      } else {
        obj.completed = false;
      }
    });

    $scope.tabs = tabs;
    console.log(tabs)

    // This sorts the topics in the order specified
    data.sort(function(a, b) {
      return parseInt(a.topicNum) - parseInt(b.topicNum);
    });

  }).
  error(function(data) {
    console.log(data);
  });

  $scope.thisAnswer = '';

  $scope.addMA = function(answer, topicId, questionId) {
    $scope.tabs.forEach(function(topic) {
      if (topic.topicId === topicId) {
        topic.questions.forEach(function(question) {
          if (question.questionId === questionId) {
            question.answer.push({theAnswer: answer});
          }
        });
      }
    });
  };
function checkAnswers(tabObj) {

    // Make sure that all the answers are filled out
    var counter = 0;
    tabObj.questions.forEach(function(obj) {
      if (Array.isArray(obj.answer)) {
        if (obj.answer.length === 0) {
          counter++;
          console.log('You have a defined answer put your array is empty');
        }
      } else if (obj.answer === undefined || obj.answer.theAnswer === '') {
        console.log('You\'re answer is not defined or has an empty string');
        counter++;
      }
    });
    return counter;
  }

  function radioButtonBuilder(rbStr) {
      var radioButtonArray = [];
      var start = 0;

      for (var x = 0; x < rbStr.length; x++) {
        if (rbStr.substr(x,1) === ',') {
          radioButtonArray.push({radio: rbStr.substring(start, x)});
          start = x + 1;
        }
        // console.log(rbStr.substr(x,1));
      }
      radioButtonArray.push({radio: rbStr.substring(start, x)});
      return radioButtonArray;
    }

  $scope.postAnswers = function(answersObj) {
    // var answersCounter = answersObj.questions.length;
    var newObj = [];
    var counter = checkAnswers(answersObj);


    var el = document.getElementById('error-message-' + answersObj.topicId);
    if (counter !== 0) {
      var index = $scope.tabs.indexOf(answersObj)
      $scope.tabs[index].completed = false;

      if (counter > 1) {
        el.innerHTML = 'You have ' + counter + ' unanswered questions. Please fill them out before continuing!';
      } else {
        el.innerHTML = 'You have 1 unanswered question. Please fill it out before continuing!';
      }
    } else {

      var index = $scope.tabs.indexOf(answersObj)
      $scope.tabs[index].completed = true;

      el.innerHTML = '<p style="color: green;">Successfully answered all questions</p>';
      answersObj.questions.forEach(function(obj) {
        newObj.push({questionId: obj.questionId, answer: obj.answer});
        console.log(newObj);
      });

      var stringObj = JSON.stringify(newObj);

      // $scope.selectedIndex++;

      var method = 'POST';
      var url = '/gitRepos/bootcamp/questions/post-questions.php';

      $http({
        method: method,
        url: url,
        data: stringObj,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).
      success(function(response) {
        $scope.codeStatus = response.data;
        console.log(response);
      }).
      error(function(response) {
        $scope.codeStatus = response || 'Request failed';
        console.log(response);
      });
    }
  };

  $scope.trustThisHTML = function(html) {
    return $sce.trustAsHtml(html);
  };

}]);
