<?php
  include '../database/config.php';
  session_start();

  $query = "SELECT * FROM topics";
  $arr = [];
  if ($result = $link->query($query)) {
    while ($topic_obj = $result->fetch_object()) {
        $topic_id = $topic_obj->topicId;
        $query_questions = "SELECT * FROM questions_table WHERE topicId = '$topic_id'";
        $questions_array = [];

        // We are getting all the related questions for each topic
        if ($result2 = $link->query($query_questions)) {
          while ($question_obj = $result2->fetch_object()) {


            $question_id = $question_obj->questionId;
            $user_id = $_SESSION['userId'];
            $answer_type = $question_obj->type;

            //If the type is MA I want to initialize the array so that I can push on it later.
            if ($question_obj->type == "MA") {
              $question_obj = (array)$question_obj;
              $question_obj['answer'] = [];
              $question_obj = (object)$question_obj;
            }


            // If there are already answers to the questions we will add them in.
            $query_answers = "SELECT * FROM begin_answers WHERE userId = '$user_id' AND questionId = '$question_id'";

            if($result3 = $link->query($query_answers)) {
              while ($answer_obj = $result3->fetch_object()) {
                $answer_question_id = $answer_obj->questionId;
                $the_answer = $answer_obj->answer;
                $the_answer_id = $answer_obj->answerId;
                $the_answer_obj = [];
                $the_answer_obj['theAnswer'] = $the_answer;
                $the_answer_obj['answerId'] = $the_answer_id;
                $the_answer_obj = (object)$the_answer_obj;
                $answer_array = [];

                $question_obj = (array)$question_obj;

                if ($answer_type == 'MA') {
                  $answer_array = $question_obj['answer'];
                  array_push($answer_array, $the_answer_obj);
                  $question_obj['answer'] = $answer_array;
                } else {
                  $question_obj['answer'] = $the_answer_obj;
                }
                // var_dump($question_obj);
                // echo "</br>";
                $question_obj = (object)$question_obj;
              }
            }
            // echo $question_obj->topicId;
            // echo $question_obj->newProperty;
            // echo $question_obj->description;
            // echo "<br>";
            array_push($questions_array, $question_obj);
          }
        }
        // adding the questions array to the topic object;
        $topic_obj = (array)$topic_obj;
        $topic_obj['questions'] = $questions_array;
        $topic_obj = (object)$topic_obj;
        array_push($arr, $topic_obj);
    }
    $json = json_encode($arr);
    echo $json;
    $result->close();
    $result2->close();
    $result3->close();
  }
  $link->close(); // Closing Connection
 ?>
