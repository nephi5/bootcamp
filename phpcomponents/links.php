<link rel="stylesheet" type="text/css" href="app.css">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/0.9.0/angular-material.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,400italic">
<link rel="stylesheet" type="text/css" href="main/main.css">
<link rel="stylesheet" type="text/css" href="header/header.css">
<link rel="stylesheet" type="text/css" href="routes/login/login-route.css">
<link rel="stylesheet" type="text/css" href="header/menu/menu-header.css">
<link rel="stylesheet" type="text/css" href="resources/resources.css">
<link rel="stylesheet" type="text/css" href="better-human/better-human.css">
<link rel="stylesheet" type="text/css" href="login-page/login-form.css">
<link rel="stylesheet" type="text/css" href="register-page/register.css" >
<link rel="stylesheet" type="text/css" href="online-edge/online-edge.css" >
<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css" >
<link rel="stylesheet" type="text/css" href="introduction-page/introduction-page.css" >
<link rel="stylesheet" type="text/css" href="routes/main/views/main-views.css" >
<link rel="stylesheet" type="text/css" href="tracking/tracking.css" >
<link rel="stylesheet" type="text/css" href="home/home.css">
<link rel="stylesheet" type="text/css" href="tracking/start-date/start-date.css">
<link rel="apple-touch-icon" sizes="57x57" href="img/game_plan_logo_2.jpg" />
<link rel="apple-touch-icon" sizes="72x72" href="img/game_plan_logo_2.jpg" />
<link rel="apple-touch-icon" sizes="114x114" href="img/game_plan_logo_2.jpg" />
<meta name="viewport" content="initial-scale=1" />
