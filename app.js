angular.module('mainApp', [
	'ui.router',
	'main-route',
	'login-route',
	'header',
	'menu-header',
	'resources',
	'ngMaterial',
	'better-human',
	'login',
	'main',
	'register.user',
	'online-edge',
	'introduction-page',
	'tracking',
	'home',
	'start-date-wizard'
]).run(function($rootScope, $state) {
		$rootScope.$state = $state;
});
