angular.module('resources', [])
	.directive('resources', ['$http', function ($http) {
		'use strict';

	return {
		templateUrl: 'resources/resources.html',
		restrict: 'E',
		link: function (scope) {

		scope.goTo = function (url, title) {
			window.open(url, '_blank');
			var resourceUsed = {
				url: url,
				title: title
			};

			$http({
          method: 'POST',
          url: 'resources/post-activity.php',
          data: JSON.stringify(resourceUsed),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
      });
		};

		$http.get('resources/get-resources.php').
			success(function(data) {
				scope.tiles = data;
			}).
			error(function(data) {
				console.log(data);
			});




		}
	};
}]);
