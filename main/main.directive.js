angular.module('main', ['service.user'])
  .directive('mainContent', ['User', '$state', function(User, $state) {
    'use strict';

    return {
      templateUrl: 'main/main.html',
      restrict: 'E',
      link: function (scope) {
        scope.authenticated = false;
        User.requestAuth("nothing").then(function (authMessage) {
          if (!authMessage) {
            $state.go('login.login');
          } else {
            scope.authenticated = true;
          }

        });
      }
    };
  }])
