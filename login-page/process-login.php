<?php
    session_start();
    include '../database/config.php';

    if (isset($_POST['submitPassword'])) {
		if (empty($_POST['userEmail']) || empty($_POST['userPassword'])) {
            $_SESSION['login_errors'] = "Username or Password is invalid 1";
		} else {

			// Define $user_email and $user_password
			$user_email = $_POST['userEmail'];
			$user_password = $_POST['userPassword'];


			// To protect MySQL injection for Security purpose
			$user_email = stripslashes($user_email);
			$user_password = stripslashes($user_password);

            // Convert user input to sha1 to be able match database query.
            $user_password = sha1($user_password);

			// SQL query to fetch information of registered users and finds user match.
			$query = "SELECT * FROM User_Table WHERE Email='$user_email'";

			$result = $link->query($query);
			$user_data = $result->fetch_array(MYSQLI_NUM);
			$row = mysqli_num_rows($result);




			if ($row == 1) {
                if ($user_data[5] == $user_password) {
                    $query = "INSERT INTO login_log (EntryTime, UserId, Email, Status) VALUES (CURRENT_TIMESTAMP, '$user_data[1]', '$user_email', 'success')";
                    $link->query($query);
                    $_SESSION['login_errors'] = "Password matched! Successfully logged in!";
                    $_SESSION['userName'] = $user_data[2];
                    $_SESSION['userEmail'] = $user_email;
                    $_SESSION['userId'] = $user_data[1];
                    $_SESSION['completedQuestions'] = $user_data[8];
                    header("location: ../questions/questions.php");

                } else {
                    $query = "INSERT INTO login_log (EntryTime, UserId, Email, Status) VALUES (CURRENT_TIMESTAMP, '$user_data[1]', '$user_email', 'wrong password')";
                    $link->query($query);
                    $_SESSION['login_errors'] = "Wrong Password or username!";
                    header("location: login.php");
                }
				// $_SESSION['login_user'] = $user_email; // Initializing Session
				// $_SESSION['user_data'] = $user_data[0];
				// $query = "UPDATE Users SET Logged_in = 1 WHERE Username = '$user_email'";
				// $link->query($query);
				// echo($_SESSION['login_user']);
			} else {
                $query = "INSERT INTO login_log (EntryTime, UserId, Email, Status) VALUES (CURRENT_TIMESTAMP, 'unkown', '$user_email', 'unknown user')";
                $link->query($query);
                $_SESSION['login_errors'] = "Wrong Password or username!";
                header("location: login.php");
			}
      $result->close()
			$link->close()
		}
	} else {
        $_SESSION['login_errors'] = "Server Error! Referesh the page and try again!";
    }
 ?>
