angular.module('login', ['service.user'])
.directive('loginForm', ['User','$state', function (User, $state) {
  'use strict';
  return {
    templateUrl: 'login-page/login.html',
    restrict: 'E',
    link: function (scope) {

      var loginObj = {};
      scope.errorMessage = '';
      scope.$state = $state;

      scope.login = function () {
        loginObj.email = scope.loginForm.email.$viewValue;
        loginObj.password = scope.loginForm.password.$viewValue;
        var stringObj = JSON.stringify(loginObj);
        User.requestAuth(stringObj).then(function (authMessage) {
          if (!authMessage) {
            scope.errorMessage = "username and/or password did not match";
          } else {
            User.requestUser().then(function (user) {
              scope.user = user;
              console.log(user);
              if(scope.user.completedQuestions === '0') {
                $state.go('login.introductionPage');
              } else {
                $state.go('main.onlineEdge');
              }
            });
          }
        });
      }
    }
  };
}]);
