angular.module('login-route', [])
	.config(function($stateProvider) {

		$stateProvider
			.state('login', {
				url: '/my',
				templateUrl: 'routes/login/login-route.html'
			}).
			state('login.login', {
				url: '/login',
				templateUrl: 'routes/login/views/login-login.html'
			})
			.state('login.register', {
				url: '/register',
				templateUrl: 'routes/login/views/login-register.html'
			})
			.state('login.introductionPage', {
				url: '/intro',
				templateUrl: 'routes/login/views/login-intro.html'
			});
	});
