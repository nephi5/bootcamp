angular.module('main-route', [])
	.config(function($stateProvider, $urlRouterProvider) {

		$urlRouterProvider.when("", "/main/home");
		$urlRouterProvider.when("/", "/main/home");
		$urlRouterProvider.otherwise('/main/home');

		$stateProvider
			.state('main', {
				url: '/main',
				templateUrl: 'routes/main/views/main.html'
			}).
			state('main.home', {
				url: '/home',
				templateUrl: 'routes/main/views/main-home.html'
			})
			.state('main.resources', {
				url: '/resources',
				templateUrl: 'routes/main/views/main-resources.html'
			})
			.state('main.betterHuman', {
				url: '/better-human',
				templateUrl: 'routes/main/views/main-better-human.html'
			})
			.state('main.onlineEdge', {
				url: '/online-edge',
				templateUrl: 'routes/main/views/main-online-edge.html'
			})
			.state('main.projects', {
				url: '/projects',
				templateUrl: 'routes/main/views/main-projects.html'
			})
			.state('main.contactManagement', {
				url: '/contact-management',
				templateUrl: 'routes/main/views/main-contact-management.html'
			})
			.state('main.tracking', {
				url: '/tracking',
				templateUrl: 'routes/main/views/main-tracking.html'
			})
			.state('main.conferenceCalls', {
				url: '/conference-calls',
				templateUrl: 'routes/main/views/main-conference-calls.html'
			});
	});
