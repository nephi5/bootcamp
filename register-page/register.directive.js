angular.module('register.user', [])
  .directive('register', ['$http', '$state', function ($http, $state) {
    'use strict'

    return {
      templateUrl: 'register-page/register.html',
      restrict: 'E',
      link: function (scope) {

        scope.registerForm;

        scope.matchPassword = function() {
          var pass1;
          var pass2;

          pass1 = document.getElementById('password1').value;
          pass2 = document.getElementById('password2').value;

          if (pass1 === pass2) {
            document.getElementById('passwordMatch').innerHTML = '<div class="label label-success">Password Match!</div>';
          } else {
            document.getElementById('passwordMatch').innerHTML = '<div class="label label-danger" ">Password don\'t match</div>';
          }
        }

        scope.registerUser = function () {
          var registerObj = JSON.stringify(scope.registerForm);
          $http({
              method: 'POST',
              url: 'register-page/post-registration.php',
              data: registerObj,
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response) {
              switch(response.data) {
                case '205':
                    scope.errorMessage = 'Successfull registration!';
                    $state.go('login.login');
                  break;
                case '450':
                  scope.errorMessage = 'Password did not match!';
                  break;
                case '451':
                  scope.errorMessage = 'User email already exists!';
                  break;
                case '452': 
                  scope.errorMessage = 'To complete registration we need your First Name';
                  break;
                case '453':
                  scope.errorMessage = 'To complete registration we need your Last Name';
                  break;
                case '454':
                  scope.errorMessage = 'To complete registration we need your Email address';
                  break;
                case '455':
                  scope.errorMessage = 'To complete registration we need your Blendfresh Id';
                  break;
                default:
                  console.log(response);
              }
          });
        }

        scope.saveToLocalStorage = function(save) {
          switch(save) {
            case 'fn':
              localStorage.firstName = scope.registerForm.firstName;
              break;
            case 'ln':
              localStorage.lastName = scope.registerForm.lastName;
              break;
            case 'e':
              localStorage.Email = scope.registerForm.email;
              break;
            case 'bi':
              localStorage.blendfreshId = scope.registerForm.blendFreshId;
              break;
          }
        };

        //  function loadLocalStorage() {
        //   if (localStorage.firstName === undefined) {localStorage.firstName = '';} else {scope.registerForm.firstName = localStorage.firstName;}
        //   if (localStorage.lastName === undefined) {localStorage.lastName = '';} else {scope.registerForm.lastName = localStorage.lastName;}
        //   if (localStorage.Email === undefined) {localStorage.Email = '';} else {scope.registerForm.email = localStorage.Email;}
        //   if (localStorage.blendfreshId === undefined) {localStorage.blendfreshId = '';} else {scope.registerForm.blendFreshId = localStorage.blendfreshId;}
        // };
        //
        // loadLocalStorage();

      }
    };
  }]);

// Save stuff to local storage so when they don't have matching passwords they can just enter the passwords.
// Also should save when there is an existing email in the database.
