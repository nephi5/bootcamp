angular.module('header', ['service.user'])
	.directive('mainHeader', ['User', function (User) {
		return {
			templateUrl: 'header/header.html',
			restrict: 'E',
			link: function (scope) {
				User.requestUser().then(function (user) {
					scope.user = user;
				});
			}
		}
	}]);
