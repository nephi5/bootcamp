angular.module('menu-header', [])
	.directive('menuHeader', ['$state', '$rootScope', function ($state, $rootScope) {
		'use strict';
		return {
			templateUrl: 'header/menu/menu-header.html',
			restrict: 'E',
			link: function (scope) {
				scope.stateName = $state.current.name;
				$rootScope.$on('$stateChangeSuccess', function () {
					scope.stateName = $state.current.name
				});

			}
		}
	}]);
