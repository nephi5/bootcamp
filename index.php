<!DOCTYPE html>
<html ng-app="mainApp">
<head>
	<?php include ('phpcomponents/links.php'); ?>
	<title>THE 90 GP</title>
</head>
<body ui-view>

	<?php include('phpcomponents/angular-js.php') ?>
	<script src="app.js"></script>
	<script src="routes/main/main-routes.js"></script>
	<script src="routes/login/login-route.js"></script>
	<script src="header/header-directive.js"></script>
	<script src="header/menu/menu-header-directive.js"></script>
	<script src="resources/resources-directive.js"></script>
	<script src="better-human/better-human-directive.js"></script>
	<script src="login-page/login.directive.js"></script>
	<script src="service/User/service.user.js"></script>
	<script src="main/main.directive.js"></script>
	<script src="register-page/register.directive.js"></script>
	<script src="online-edge/online-edge.directive.js"></script>
	<script src="introduction-page/introduction-page.directive.js"></script>
	<script src="tracking/tracking.directive.js"></script>
	<script src="home/home.directive.js"></script>
	<script src="tracking/start-date/start-date.controller.js"></script>
</body>
</html>
