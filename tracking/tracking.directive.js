angular.module('tracking', [])
.directive('tracking', ['$http', '$mdDialog', function($http, $mdDialog) {
  'use strict';

  return {
    templateUrl: 'tracking/tracking.html',
    restrict: 'E',
    link: function(scope) {

      var selected = [];
      var currentDate;
      scope.total = 0;

      function calcScore() {
        scope.total = 0;
        selected.forEach(function(item) {
          scope.total += parseInt(item.points);
        });
      }

      scope.getData = function(date) {
        $http({
          method: 'POST',
          url: 'tracking/get-tracking.php',
          data: JSON.stringify({queryDate: date}),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
          success(function(data) {
            selected = [];
            data.trackingItems.forEach(function(obj) {
              if (obj.isSelected) selected.push(obj);
            })

            scope.tracking = data.trackingItems;
            scope.todaysDate = data.todaysDate;
            scope.gameDayNum = data.gameDayNum;
            calcScore();
          }).
          error(function(data) {
            console.log(data);
          });
      };

      function startDate() {
        $http.get('tracking/start-date/get-start-date.php').success(function(data) {
          console.log(data);
          if (data.BeginDate === '0000-00-00') {
            $mdDialog.show({
              templateUrl: 'tracking/start-date/start-date.html',
              controller: 'startDateWizard'
            }).then(function(startDate) {
              if (startDate) {
                startDate = correctDateFormat(startDate);
                submitStartDate({startDate: startDate});
              }
            })
          } else {
            var todaysDate = new Date();
            var today = correctDateFormat(todaysDate);
            currentDate = today;
            scope.getData(today);
          }
        });
      }

      startDate();

      scope.changeDay = function(direction) {
        var newDate = new Date(currentDate);
        var dateNum = newDate.getTime();
        if (direction === 'back') dateNum -= 1;
        else if (direction === 'forward') dateNum += 86400000;

        newDate = new Date(dateNum);
        newDate = correctDateFormat(newDate);
        scope.getData(newDate);
        currentDate = newDate;
      }

      scope.toggle = function(item) {
        var idx = selected.indexOf(item);
        if (idx > -1) { selected.splice(idx, 1);
        } else { selected.push(item);}

        calcScore();

        item.gameDayNum = scope.gameDayNum;
        item.todaysDate = scope.todaysDate;
        $http({
          method: 'POST',
          url: 'tracking/post-tracking-changes.php',
          data: JSON.stringify(item),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(response) {
          // console.log(response);
        }).
        error(function(response) {
          // console.log(response);
        });
      }

      function submitStartDate(startDateObj) {
        $http({
          method: 'POST',
          url: 'tracking/start-date/post-start-date.php',
          data: JSON.stringify(startDateObj),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
          console.log(response);
        });
      }

      function correctDateFormat(date) {
        var correctFormat = date.getFullYear();
        correctFormat += '-' + (date.getMonth() + 1) + '-';
        correctFormat += date.getDate();
        return correctFormat;
      };
    }
  };
}]);
