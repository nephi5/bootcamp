angular.module('start-date-wizard', ['ngMaterial'])
.controller('startDateWizard', ['$mdDialog', '$scope', function($mdDialog, $scope) {
  'use strict';

  $scope.startDate = {};

  $scope.closeDialog = function() {
    $mdDialog.hide();
  };

  $scope.submitStartDate = function() {
    $mdDialog.hide($scope.startDate.date);

  }
}])
