<?php
  include '../database/config.php';
  session_start();

  $data = json_decode(file_get_contents("php://input"));
  //------------------------------------------- MAKE SURE TO REMOVE THE BLOCK BELOW ------------------------------------
  // $data = [];
  // $data['queryDate'] = "2015-05-22";
  // $data = (object)$data;
  //------------------------------------------- MAKE SURE TO REMOVE THE BLOCK ABOVE ------------------------------------

  if(isset($data)) {
    $todays_date = $data->queryDate;
  } else {
    $todays_date = date('Y-m-d');
  }

  $user_id = $_SESSION['userId'];
  $query_user = "SELECT * FROM User_Table WHERE UserId = '$user_id'";

  // Query for the begin date and according to that calc the game_day_num
  if($result3 = $link->query($query_user)) {
    $data = $result3->fetch_assoc();
    $begin_date = $data['BeginDate'];
    $datetime1 = new DateTime($begin_date);
    $datetime2 = new DateTime($todays_date);
    $interval = $datetime1->diff($datetime2);
    $game_day_num = $interval->format('%a');
  }

  $result3->close();

  $tracking_data_arr = [];
  $tracking_obj = [];

  $join_tracking = "SELECT * FROM tracking_items TRI
  LEFT JOIN tracking_data TRD
  ON TRI.id = TRD.trackingItemId
  WHERE TRD.date = '$todays_date' AND TRD.userId = '$user_id'";



  /* This BLOCK will fill the tracking_data_arr array with all the items the query returns
  This array will be used to match the tracking items, this means for this date ($todays_date) with user ($user_id)
  tracked something and so in the HTML it will come back selected.*/
  if($join_result = $link->query($join_tracking)) {
    while ($tracking_row = $join_result->fetch_object()) {
      array_push($tracking_data_arr, $tracking_row);
    }
  }
  $join_result->close();



  $arr = [];
  $count_tracking_data_arr = count($tracking_data_arr);

  $query = "SELECT * FROM tracking_items";

  if ($result = $link->query($query)) {
    while ($tracking_item_obj = $result->fetch_object()) {
      if ($count_tracking_data_arr > 0) {
        foreach ($tracking_data_arr as &$value) {
          if($value->trackingItemId == $tracking_item_obj->id) {
            $tracking_item_obj = (array)$tracking_item_obj;
            $tracking_item_obj['isSelected'] = true;
            $tracking_item_obj = (object)$tracking_item_obj;
            break;
          } else {
            $tracking_item_obj = (array)$tracking_item_obj;
            $tracking_item_obj['isSelected'] = false;
            $tracking_item_obj = (object)$tracking_item_obj;
          }
        }
      } else {
        $tracking_item_obj = (array)$tracking_item_obj;
        $tracking_item_obj['isSelected'] = false;
        $tracking_item_obj = (object)$tracking_item_obj;
      }
      array_push($arr, $tracking_item_obj);
    }

    $tracking_obj['trackingItems'] = $arr;
    $tracking_obj['gameDayNum'] = $game_day_num;
    $tracking_obj['todaysDate'] = $todays_date;
    $tracking_obj['beginDate'] = $begin_date;
    $tracking_obj = (object)$tracking_obj;
    $json = json_encode($tracking_obj);
    echo $json;
    $result->close();
  }
  $link->close();
 ?>
