<?php
    include 'config.php';
    $connection = new mysqli ($servername_config, $username_config, $password_config, $dbname_config);
    $query2 = "SELECT * FROM User_Table";
    $result = mysqli_query($connection, $query2);
    $html = "";


    if (mysqli_num_rows($result) > 0) {
        $html = '<table id="display-users"><tbody><tr><th>User ID</th><th>Name</th><th>Email</th><tr>';
        while($row = mysqli_fetch_assoc($result)) {
                $html = $html.'<tr><td>'. $row['UserId'].'</td><td>'. $row["FirstName"]. " " . $row["LastName"] .'</td><td>'. $row["Email"].'</td></tr>';
        }
    } else {
        echo "0 results";
    }
    $html = $html.'</tbody></table>';
    // mysqli_close_connection()
?>
